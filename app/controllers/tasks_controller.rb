# frozen_string_literal: true

require 'json'

# Managing and syncing of tasks
class TasksController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    @tasks = Task.select(:id, :name, :checked)
  end

  def sync
    changes = JSON.parse(request.body.read)

    render json: { created: TaskSyncer.new(changes).call }
  end
end
