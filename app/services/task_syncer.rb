# frozen_string_literal: true

# Syncing of task changes, including additions, removals, and changes
class TaskSyncer
  def call
    created = handle_added(@changes['added'])
    @changes['removed'].each { |id| Task.destroy_by(id:) }

    @changes['changed'].each do |task|
      Task.find(task['id']).update(task.transform_keys(&:underscore).symbolize_keys)
    end

    created
  end

  private

  def handle_added(added)
    created = []
    added.each do |task|
      new_task = Task.create!(task.transform_keys(&:underscore).symbolize_keys.except(:id))
      created << { phantom: task['id'], new_id: new_task.id }
    end
    created
  end

  def initialize(changes)
    @changes = changes
  end
end
