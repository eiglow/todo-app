import React from 'react';
import { createRoot } from 'react-dom/client';

/**
 * Mounts a React component on the element matching the ID given
 * @param {React.Component} Component - The React component to render
 * @param {string} id - the ID of the HTML element on which to mount the component
 */
export default function mountComponent(Component: React.FunctionComponent, id: string) {
  document.addEventListener('DOMContentLoaded', () => {
    const node = document.getElementById(id);

    if (node) {
      const props = JSON.parse(node.getAttribute('data-props'));
      const root = createRoot(node);
      root.render(React.createElement(Component, props));
    }
  });
}

/**
 * Mounts a React component on every element matching the class given
 * @param {React.Component} Component - The React component to render
 * @param {string} className - the class of the HTML elements on which to mount the component
 */
function mountComponents(Component: React.FunctionComponent, className: string) {
  document.addEventListener('DOMContentLoaded', () => {
    const nodes = document.getElementsByClassName(className);

    for (let i = 0; i < nodes.length; i++) {
      const props = JSON.parse(nodes[i].getAttribute('data-props'));
      const root = createRoot(nodes[i]);
      root.render(React.createElement(Component, props));
    }
  });
}
export { mountComponents };
