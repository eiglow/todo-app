export type FlatTask = {
  id: number,
  name: string,
  checked: boolean,
  sortOrder: number
};

export type MapTask = Omit<FlatTask, 'id'>;

export type Diff = {
  added: FlatTask[],
  removed: number[],
  changed: FlatTask[]
};

export type CreatedRecord = {
  phantom: number,
  new_id: number
}