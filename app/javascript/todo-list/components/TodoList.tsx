import React, { FC, useState, useEffect } from 'react';
import { omit, isEqual, cloneDeep } from 'lodash';
import Task from './Task';
import NewTaskButton from './NewTaskButton';
import { FlatTask, MapTask, Diff, CreatedRecord } from '../../shared/Types';
import './TodoList.scss';

interface TodoListProps {
  tasks: FlatTask[];
}

const diff = (oldMap: Map<number, MapTask>, newMap: Map<number, MapTask>) => {
  const difference: Diff = {
    added: [] as FlatTask[],
    removed: [] as number[],
    changed: [] as FlatTask[]
  };
  const keysOld = Array.from(oldMap.keys());
  const keysNew = Array.from(newMap.keys());

  difference.removed = keysOld.filter((key) => !keysNew.includes(key));

  keysNew
    .filter((key) => !keysOld.includes(key))
    .forEach((key) => {
      difference.added.push({ id: key, ...newMap.get(key) } as FlatTask);
    });

  keysOld
    .filter((key) => keysNew.includes(key))
    .forEach((key) => {
      if (!isEqual(oldMap.get(key), newMap.get(key))) {
        difference.changed.push({ id: key, ...newMap.get(key) } as FlatTask);
      }
    });

  return difference;
};

// Converts FlatTasks to MapTasks
const ingestTasks = (tasks: FlatTask[]) =>
  new Map(tasks.map((task: FlatTask) => [task.id, omit(task, 'id') as MapTask]));

const TodoList: FC<TodoListProps> = (props) => {
  const [tasks, setTasks] = useState(ingestTasks(props.tasks));
  const [originalTasks, setOriginalTasks] = useState(ingestTasks(props.tasks));
  const [persisting, setPersisting] = useState(false);

  const persist = async (difference: Diff) => {
    setPersisting(true);
    const request = {
      method: 'POST',
      body: JSON.stringify(difference)
    };
    const response = await (await fetch('/sync', request)).json();

    if (response.created?.length > 0) {
      setTasks((prevTasks) => {
        response.created.forEach((created: CreatedRecord) => {
          const clonedTask = cloneDeep(prevTasks.get(created.phantom));
          prevTasks.delete(created.phantom);
          prevTasks.set(created.new_id, clonedTask);
        });
        setOriginalTasks(structuredClone(prevTasks));
        return structuredClone(prevTasks);
      });
    } else {
      setOriginalTasks(structuredClone(tasks));
    }

    setPersisting(false);
  };

  useEffect(() => {
    if (persisting) return;
    const difference = diff(originalTasks, tasks);

    if (difference.added.length > 0 || difference.removed.length > 0 || difference.changed.length > 0) {
      persist(difference);
    }
  }, [persisting, tasks, originalTasks]);

  const createTask = () => {
    setTasks((prevTasks) => {
      prevTasks.set(Date.now(), { name: '', checked: false, sortOrder: prevTasks.size + 1 });
      return new Map(prevTasks);
    });
  };

  const changeTaskField = <T extends keyof MapTask>(id: number, field: T, newValue: MapTask[T]) => {
    setTasks((prevTasks: Map<number, MapTask>) => {
      const task: MapTask = prevTasks.get(id);
      task[field] = newValue;
      return new Map(prevTasks);
    });
  };

  const remove = (id: number) => {
    setTasks((prevTasks) => {
      prevTasks.delete(id);
      return new Map(prevTasks);
    });
  };

  const tableBody = Array.from(tasks).map(([id, task]) => (
    <Task
      key={id}
      id={id}
      name={task.name}
      checked={task.checked}
      changeTaskField={changeTaskField}
      remove={remove}
      disabled={persisting}
    />
  ));

  return (
    <div className="todoList">
      <h1>Todo List</h1>
      <table className="todoTable"><tbody>
        {tableBody}
        <NewTaskButton
          createTask={createTask}
          disabled={persisting}
        />
      </tbody></table>
    </div>
  );
};

export default TodoList;