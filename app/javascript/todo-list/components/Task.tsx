import React, { FC, useState, useEffect, useRef } from 'react';
import { MapTask } from '../../shared/Types';

interface TaskProps {
  name: string,
  checked: boolean,
  id: number,
  changeTaskField: <T extends keyof MapTask>(id: number, field: T, newValue: MapTask[T]) => void,
  remove: (id: number) => void,
  disabled: boolean
}

const Task: FC<TaskProps> = (props) => {
  const [hasDefocused, setHasDefocused] = useState(false);
  const [name, setName] = useState(props.name);
  const textBoxEl = useRef(null);

  useEffect(() => {
    if (hasDefocused) {
      setHasDefocused(false);
      if (name !== props.name) {
        props.changeTaskField(props.id, 'name', name);
      }
    }

    // Setting height to "auto" here is needed, otherwise the scrollHeight will never decrease when text is deleted
    textBoxEl.current.style.height = 'auto';
    textBoxEl.current.style.height = textBoxEl.current.scrollHeight.toString() + 'px';
  }, [hasDefocused, name, props]);

  return (
    <tr>
      <td className="cell-button">
        <input
          type="checkbox"
          defaultChecked={props.checked}
          onChange={e => props.changeTaskField(props.id, 'checked', e.target.checked)}
          disabled={props.disabled}
        />
      </td>
      <td>
        <textarea
          ref={textBoxEl}
          className="task-name-textbox"
          onBlur={() => setHasDefocused(true)}
          onChange={e => setName(e.target.value)}
          onFocus={e => e.target.select()}
          value={name}
          rows={1}
          placeholder="Empty"
          spellCheck="false"
          disabled={props.disabled}
        >
        </textarea>
      </td>
      <td className="cell-button">
        <button
          onClick={() => props.remove(props.id)}
          disabled={props.disabled}
        >X</button>
      </td>
    </tr>
  );
};

export default Task;