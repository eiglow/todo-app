import React, { FC, MouseEvent } from 'react'

interface NewTaskButtonProps {
  createTask: (event: MouseEvent<HTMLButtonElement>) => void,
  disabled: boolean
}

const NewTaskButton: FC<NewTaskButtonProps> = ({ createTask, disabled }) => (
  <tr>
    <td className="cell-button"></td>
    <td>
      <button
        onClick={createTask}
        disabled={disabled}
      >
        + New task
      </button>
    </td>
    <td></td>
  </tr>
);

export default NewTaskButton;