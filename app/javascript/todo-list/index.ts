import mountComponent from '../build-utils';
import Component from './components/TodoList';
mountComponent(Component, 'js-mount--todo-list');