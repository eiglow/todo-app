# Todo app by Alexander Robb

## Setup:

1. Install `pm2`: `yarn global add pm2`
1. Install and set up `asdf`: https://asdf-vm.com/guide/getting-started.html
2. Add the ruby plugin: `asdf plugin add ruby`
3. Install ruby: `asdf install`
4. Install gems: `bundle install`
5. Install yarn packges: `yarn`
6. Run migrations: `rails db:migrate`
