module.exports = {
  apps: [
    {
      name: 'todo-app',
      command: 'bundle exec rails s'
    },
    {
      name: 'shakapacker-dev-server',
      script: './bin/shakapacker-dev-server',
      exec_interpreter: 'ruby',
      exec_mode: 'fork_mode'
    },
  ]
};