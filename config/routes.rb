# frozen_string_literal: true

Rails.application.routes.draw do
  root 'tasks#index'
  post :sync, to: 'tasks#sync'
end
