class AddSortOrderToTasks < ActiveRecord::Migration[6.1]
  def change
    add_column :tasks, :sort_order, :number
  end
end
