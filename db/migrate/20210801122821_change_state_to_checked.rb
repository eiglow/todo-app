class ChangeStateToChecked < ActiveRecord::Migration[6.1]
  def change
    rename_column :tasks, :state, :checked
  end
end
