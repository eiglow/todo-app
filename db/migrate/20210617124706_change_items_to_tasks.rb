class ChangeItemsToTasks < ActiveRecord::Migration[6.1]
  def change
    rename_table :items, :tasks
  end
end
